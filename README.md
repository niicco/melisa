# **Melisa** #
## 3º Lugar en Hackaton "Mercado Libre - Microsoft 2016" ##

Proyecto para la Hackathon de Mercadolibre y Microsoft con el que obtuvimos el 3 lugar. Hecho con Node.js, Express, Angular, la API de Mercado Libre y LUIS (Language Understanding Intelligent Service).
Nuestro equipo tuvo en cuenta que la velocidad de respuesta tiene efectos en la conversión de venta y pensamos una solución llamada "Melisa" que utilizando Microsoft Luis a medida que se tipea se buscan posibles respuestas automáticas relacionadas a la publicación sobre la que se está escribiendo, y así evitar que se pregunte algo redundante.


Ejemplos de frases y palabras que reconoce:

* color
* que color
* en que colores
* colores
* precio
* cual es el precio
* cual es el p
* quiero saber precio
* quiero precio
* garantia
* que garantia

[Demo](https://meli-sa.herokuapp.com)

[Link Mercado Libre con Melisa en lista de ganadores Hackathon 2016](http://developers.mercadolibre.com/events/hackathon-mercado-libre-microsoft/)